import { Component } from '@angular/core';
import { Platform} from 'ionic-angular';
import { InAppBrowser } from 'ionic-native';

declare var cordova:any;
 
 
@Component({
    templateUrl: 'home.html'
})
export class HomePage {
	
    static get parameters() {
        return [[Platform]];
    }
 
    constructor(public platform: Platform) {
		this.platform.ready().then(() => {
            window.open('http://www.google.com', "_system", "location=true");
        });
	}
 
}